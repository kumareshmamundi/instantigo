import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AuthComponent } from './auth/auth.component';
import { FlightComponent } from './flight/flight.component';
import { SearchComponent } from './flight/search/search.component';
import { FlightListComponent } from './flight/flight-list/flight-list.component';
import { FlightDetailsComponent } from './flight/flight-details/flight-details.component';
import { FlightBookingComponent } from './flight/flight-booking/flight-booking.component';
import { PaymentComponent } from './flight/payment/payment.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule} from '@angular/forms';
import { MatButtonModule,
         MatToolbarModule,
         MatMenuModule,
         MatInputModule,
         MatCardModule

         } from '@angular/material';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AuthComponent,
    FlightComponent,
    routingComponents
    SearchComponent,
    FlightListComponent,
    FlightDetailsComponent,
    FlightBookingComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatInputModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
